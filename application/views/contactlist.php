
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Table List</a>
                </div>
                
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Contact List</h4>
                                
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                    </thead>
                                    <tbody id="contactid">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                


                </div>
            </div>
        </div>

        


    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?=base_url('assets/js/jquery.3.2.1.min.js" type="text/javascript');?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js" type="text/javascript');?>"></script>

    <!--  Charts Plugin -->
    <script src="<?=base_url('assets/js/chartist.min.js');?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="<?=base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0');?>"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="<?=base_url('assets/js/demo.js');?>"></script>
    <script>
        var server = 'http://192.168.0.113/ci3/index.php';
        function refreshcon(){
            $('#contactid').html('');
            $.ajax({
                    url: server + '/email/check_contact',
                    type: 'post',
                    data: {},
                    dataType:"json",        
                    success: function( data ){
                        for(var i = 0;i < data.data.length;i++){
                            $('#contactid').append("<tr><td>"+ data.data[i].id + "</td><td>"+ data.data[i].name + "</td><td>"+
                             data.data[i].email + "</td>/<tr>");
                        }POST
                    }
                });
        }
        refreshcon();
    
    </script>


</html>
