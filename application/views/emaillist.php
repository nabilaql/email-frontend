
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Table List</a>
                </div>
                
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">SCHEDULED EMAIL (WAITING)</h4>
                               
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Created by</th>
                                        <th>Send at</th>
                                        <th>Scheduled at</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody id="emailid">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                


                </div>
            </div>
        </div>
          <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">ALL MAILS</h4>
                               
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Created by</th>
                                        <th>Send at</th>
                                        <th>Scheduled at</th>
                                         <th>Status</th>
                                    </thead>
                                    <tbody id="allid">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                


                </div>
            </div>
        </div>

        


    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?=base_url('assets/js/jquery.3.2.1.min.js" type="text/javascript');?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js" type="text/javascript');?>"></script>

    <!--  Charts Plugin -->
    <script src="<?=base_url('assets/js/chartist.min.js');?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="<?=base_url('assets/js/light-bootstrap-dashboard.js?v=1.4.0');?>"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="<?=base_url('assets/js/demo.js');?>"></script>
    <script>
        var server = 'http://192.168.0.113/ci3/index.php';
        function refresh(){
            $('#emailid').html('');
            $.ajax({
                    url: server + '/email/check_all',
                    type: 'post',
                    data: {},
                    dataType:"json",        
                    success: function( data ){
                        for(var i = 0;i < data.data.length;i++){
                            $('#emailid').append("<tr><td>"+ data.data[i].id + "</td><td>"+ data.data[i].title + "</td><td>"+
                             data.data[i].created_at + "</td><td>"+ data.data[i].send_at + "</td><td>"+ data.data[i].scheduled_at + "</td><td>"
                             + data.data[i].status + "</td>/<tr>");
                        }POST
                    }
                });
        }
        refresh();
        setInterval(function(){
                $.ajax({
                url: server + '/email/check',
                type: 'post',
                data: {},
                dataType:"json",        
                success: function( data ){
                   for(var i=0;i<data.data.length;i++ ){
                        $.ajax({
                            url: server + '/email/send',
                            type: 'post',
                            data: {'id':data.data[i].id,'subject':data.data[i].email_title,'content':data.data[i].email_content},
                            dataType:"json",        
                            success: function( data ){
                               
                      refresh();
                      refreshall();
                            }
                        });
                   }
                }
            });
        },10000);
        var server = 'http://192.168.0.113/ci3/index.php';
        function refreshall(){
            $('#allid').html('');
            $.ajax({
                    url: server + '/email/list_all',
                    type: 'post',
                    data: {},
                    dataType:"json",        
                    success: function( data ){
                        for(var i = 0;i < data.data.length;i++){
                            $('#allid').append("<tr><td>"+ data.data[i].id + "</td><td>"+ data.data[i].title + "</td><td>"+
                             data.data[i].created_at + "</td><td>"+ data.data[i].send_at + "</td><td>"+ data.data[i].scheduled_at + "</td><td>"
                             + data.data[i].status + "</td>/<tr>");
                        }POST
                    }
                });
        }
        refreshall();
    </script>


</html>
